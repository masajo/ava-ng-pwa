let express = require('express');
let bodyParser = require('body-parser');
let cors = require('cors');
let webPush = require('web-push');


// * 1. Instanciar NUESTRA APLICACIÓN EXPRESS
let app = express();

app.use(bodyParser.urlencoded({
  extended: false
}));

// * 2. VAMOS A DECIR QUE NUESTRA APLICACIÓN CONSUME - JSON
app.use(bodyParser.json());

// * 3. CONFIGURACIÓN DE CORS
// Vamos a poner que la aplicación puede recibir peticiones de cualquier origen
app.use(cors());

// * 4. PLANTEAMIENTO DE ENDPOINTS DE NUESTRA APLICACIÓN EXPRESS

// Endpoint Raíz: GET --> localhost:3000/
app.get('/', (req, res) => {

  res.status(200).json({
    mensaje: "API funcionando correctamente"
  });

});


// Enpoint para suscribirse y que el cliente reciba una notificación push
app.post('/subscribe', (req, res) => {

  // Obtener el body de un post
  let body = req.body;
  let public_key = 'BFx4P1g7EJQ2JIZ3yhtU102gAOw3NN6nl8uwt3a4O4k2TLNtqPf-X-I1v6GKMJvJvQq5RtrpsmTSW1QZ4R61sI0'
  let private_key = 'B0aYlXSwNqMOdHMhr5dEunRAYmzpSRw-s0zoMhJJiIk'

  // Configuración deconexión de mensajería entre cliente/servidor
  // a través de clave pública y clave privada
  webPush.setVapidDetails(
    'mailto:martin@imaginagroup.com',
    public_key,
    private_key
  );

  // Creamos la notificación que vamos a mandar al cliente que estará suscrito
  let notification = JSON.stringify(
    {
      "notification": {
        "title": "Martín API",
        "body": "Gracias por suscribirte a nuestra newsletter",
        "icon": "https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-bell-512.png"
      }
    }
  );

  // Enviar la notificación y seguido un mensaje de éxito
  Promise.resolve(
    webPush.sendNotification(body, notification)
  ).then(
    () => {
      res.status(200).json(
        {
          mensaje: "Notificación enviada con éxito"
        }
      )
    }
  ).catch((error) => {
    console.log(`Error de servidor al enviar notificación: ${error}`);
    res.status(500).json(
      {
        mensaje: 'Ha ocurrido un error',
        error: error
      }
    )
  })


})




// * 5. DESPLIEGUE DE APLICACIÓN EN LOCALHOST:3000
app.listen(3000, () => {
  console.log('Aplicación Express escuchando en el puerto 3000...')
});


