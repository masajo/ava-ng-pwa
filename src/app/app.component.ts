import { Component, OnInit } from '@angular/core';
import { SwUpdate, SwPush } from '@angular/service-worker';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-pwa';
  frase = '';

  constructor(private _swUpdate: SwUpdate, private _swPush: SwPush, private _dataService:DataService){}

  ngOnInit(){
    // Al iniciar el componente recargar la caché de la aplicación a través del Service Worker
    this.recargarCache();
  }

  recargarCache(){
    if(this._swUpdate.isEnabled){
      this._swUpdate.available.subscribe(
        (event) => {
          if (confirm('Hay una nueva versión disponible. ¿Desea cargarla?')) {
            this._swUpdate.activateUpdate().then(
              () => window.location.reload()
            ).catch(
              (error) => alert(`Ha habido in error al recargar la página: ${error}`)
            )
          }
        },
        (error) => alert(`Ha habido un error al cargar la nueva versión de la app: ${error}`),
        () => console.log('Nueva versión de la app cargada')
      )
    }
  }


  obtenerFrase(){
    this._dataService.obtenerFraseAleatoria().subscribe(
      (respuesta) => {
        this.frase = respuesta.value
      },
      (error) => alert(`Ha habido un error al obtener una frase nueva: ${error}`),
      () => console.log('Nueva frase obtenida con éxito')
    )
  }

  suscribirseNewsLetter(){

    if (this._swUpdate.isEnabled) {
      this._swPush.requestSubscription(
        {
          serverPublicKey: 'BFx4P1g7EJQ2JIZ3yhtU102gAOw3NN6nl8uwt3a4O4k2TLNtqPf-X-I1v6GKMJvJvQq5RtrpsmTSW1QZ4R61sI0'
        }
      ).then(
        (suscripcionPush: PushSubscription) => {
          this._dataService.suscribirse(suscripcionPush).subscribe();
        }
      ).catch((error) => alert("Error"))

    }
  }

}

